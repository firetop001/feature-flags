# feature-flag-status

This app was created to allow us to filter and find feature flags more easily.

It runs a scheduled pipeline every 8 hours that fetches and parses all the [yaml feature flags](https://gitlab.com/gitlab-org/gitlab/-/tree/master/config/feature_flags/development) in the main GitLab repo.
This means the data may be up to 8 hours out of date, but it has the benefit of running a lot faster.

## Project setup
```
yarn install
```

### Gets Feature Flags, Parse, Compiles and hot-reloads for development
```
sh bin/getFlags.sh
node bin/parseFlags.js
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```
