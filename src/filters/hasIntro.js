const hasIntro = (flags, filter) =>
  typeof filter === "boolean"
    ? flags.filter(({ introduced_by_url }) => !!introduced_by_url === filter)
    : flags;

export default hasIntro;
