const parseFilters = (terms, filterType) =>
  terms.filter(({ type }) => type === filterType).map(({ value }) => value);

export default parseFilters;
